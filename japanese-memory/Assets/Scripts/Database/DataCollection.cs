﻿using System;
using System.Collections.Generic;

[Serializable]
public abstract class DataCollection
{
    protected List<IData> List { get; private set; }

    public DataCollection()
    {
        List = new List<IData>(); Initialize();
    }

    #region Properties
    public IData[] Collection
    {
        get { return List.ToArray(); }
    }
    #endregion

    private void Initialize()
    {
        for (int i = 0; i < 100; i++)
        {
            IData data = GetData(i);
            if (data != null)
                List.Add(data);
            else
                break;
        }
    }

    protected abstract IData GetData(int id);
}
