﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Data : IData
{
    public Dictionary<DataType, string> Dictionary = new Dictionary<DataType, string>();
    public Sprite Sprite { get; set; }
    public Category Category { get; set; }

    public string Text(DataType type)
    {
        if (Dictionary.ContainsKey(type))
            return Dictionary[type];
        return "";
    }

    public bool Contains(string text)
    {
        return Dictionary.ContainsValue(text);
    }

    public bool Contains(Sprite sprite)
    {
        return Sprite == sprite;
    }

    public bool Contains(DataType type)
    {
        return Dictionary.ContainsKey(type);
    }
}

public enum DataType
{
    Sprite,
    Kanji,
    Hiragana,
    Katakana,
    Sufix
}

public enum Category
{
    None,
    N1,
    N2,
    N3,
    N4,
    N5,
    All
}

public interface IData
{
    string Text(DataType type);
    Sprite Sprite { get; }

    bool Contains(DataType type);
    bool Contains(string text);
    bool Contains(Sprite sprite);
}
