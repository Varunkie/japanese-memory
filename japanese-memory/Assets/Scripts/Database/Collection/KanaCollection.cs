﻿public class KanaCollection : DataCollection
{
    protected override IData GetData(int id)
    {
        Data data = new Data(); data.Category = Category.N5;
        switch (id)
        {
            case 0:
                data.Dictionary.Add(DataType.Hiragana, "あ");
                data.Dictionary.Add(DataType.Katakana, "ア");
                data.Sprite = null;
                return data;
            case 1:
                data.Dictionary.Add(DataType.Hiragana, "い");
                data.Dictionary.Add(DataType.Katakana, "イ");
                data.Sprite = null;
                return data;
            case 2:
                data.Dictionary.Add(DataType.Hiragana, "う");
                data.Dictionary.Add(DataType.Katakana, "ウ");
                data.Sprite = null;
                return data;
            case 3:
                data.Dictionary.Add(DataType.Hiragana, "え");
                data.Dictionary.Add(DataType.Katakana, "エ");
                data.Sprite = null;
                return data;
            case 4:
                data.Dictionary.Add(DataType.Hiragana, "お");
                data.Dictionary.Add(DataType.Katakana, "オ");
                data.Sprite = null;
                return data;
            case 5:
                data.Dictionary.Add(DataType.Hiragana, "か");
                data.Dictionary.Add(DataType.Katakana, "カ");
                data.Sprite = null;
                return data;
            case 6:
                data.Dictionary.Add(DataType.Hiragana, "き");
                data.Dictionary.Add(DataType.Katakana, "キ");
                data.Sprite = null;
                return data;
            case 7:
                data.Dictionary.Add(DataType.Hiragana, "く");
                data.Dictionary.Add(DataType.Katakana, "ク");
                data.Sprite = null;
                return data;
            case 8:
                data.Dictionary.Add(DataType.Hiragana, "け");
                data.Dictionary.Add(DataType.Katakana, "ケ");
                data.Sprite = null;
                return data;
            case 9:
                data.Dictionary.Add(DataType.Hiragana, "こ");
                data.Dictionary.Add(DataType.Katakana, "コ");
                data.Sprite = null;
                return data;
            case 10:
                data.Dictionary.Add(DataType.Hiragana, "さ");
                data.Dictionary.Add(DataType.Katakana, "サ");
                data.Sprite = null;
                return data;
            case 11:
                data.Dictionary.Add(DataType.Hiragana, "し");
                data.Dictionary.Add(DataType.Katakana, "シ");
                data.Sprite = null;
                return data;
            case 12:
                data.Dictionary.Add(DataType.Hiragana, "す");
                data.Dictionary.Add(DataType.Katakana, "ス");
                data.Sprite = null;
                return data;
            case 13:
                data.Dictionary.Add(DataType.Hiragana, "せ");
                data.Dictionary.Add(DataType.Katakana, "セ");
                data.Sprite = null;
                return data;
            case 14:
                data.Dictionary.Add(DataType.Hiragana, "そ");
                data.Dictionary.Add(DataType.Katakana, "ソ");
                data.Sprite = null;
                return data;
            case 15:
                data.Dictionary.Add(DataType.Hiragana, "た");
                data.Dictionary.Add(DataType.Katakana, "タ");
                data.Sprite = null;
                return data;
            case 16:
                data.Dictionary.Add(DataType.Hiragana, "ち");
                data.Dictionary.Add(DataType.Katakana, "チ");
                data.Sprite = null;
                return data;
            case 17:
                data.Dictionary.Add(DataType.Hiragana, "つ");
                data.Dictionary.Add(DataType.Katakana, "ツ");
                data.Sprite = null;
                return data;
            case 18:
                data.Dictionary.Add(DataType.Hiragana, "て");
                data.Dictionary.Add(DataType.Katakana, "テ");
                data.Sprite = null;
                return data;
            case 19:
                data.Dictionary.Add(DataType.Hiragana, "と");
                data.Dictionary.Add(DataType.Katakana, "ト");
                data.Sprite = null;
                return data;
            case 20:
                data.Dictionary.Add(DataType.Hiragana, "な");
                data.Dictionary.Add(DataType.Katakana, "ナ");
                data.Sprite = null;
                return data;
            case 21:
                data.Dictionary.Add(DataType.Hiragana, "に");
                data.Dictionary.Add(DataType.Katakana, "ニ");
                data.Sprite = null;
                return data;
            case 22:
                data.Dictionary.Add(DataType.Hiragana, "ぬ");
                data.Dictionary.Add(DataType.Katakana, "ヌ");
                data.Sprite = null;
                return data;
            case 23:
                data.Dictionary.Add(DataType.Hiragana, "ね");
                data.Dictionary.Add(DataType.Katakana, "ネ");
                data.Sprite = null;
                return data;
            case 24:
                data.Dictionary.Add(DataType.Hiragana, "の");
                data.Dictionary.Add(DataType.Katakana, "ノ");
                data.Sprite = null;
                return data;
            case 25:
                data.Dictionary.Add(DataType.Hiragana, "は");
                data.Dictionary.Add(DataType.Katakana, "ハ");
                data.Sprite = null;
                return data;
            case 26:
                data.Dictionary.Add(DataType.Hiragana, "ひ");
                data.Dictionary.Add(DataType.Katakana, "ヒ");
                data.Sprite = null;
                return data;
            case 27:
                data.Dictionary.Add(DataType.Hiragana, "ふ");
                data.Dictionary.Add(DataType.Katakana, "フ");
                data.Sprite = null;
                return data;
            case 28:
                data.Dictionary.Add(DataType.Hiragana, "へ");
                data.Dictionary.Add(DataType.Katakana, "ヘ");
                data.Sprite = null;
                return data;
            case 29:
                data.Dictionary.Add(DataType.Hiragana, "ほ");
                data.Dictionary.Add(DataType.Katakana, "ホ");
                data.Sprite = null;
                return data;
            case 30:
                data.Dictionary.Add(DataType.Hiragana, "ま");
                data.Dictionary.Add(DataType.Katakana, "マ");
                data.Sprite = null;
                return data;
            case 31:
                data.Dictionary.Add(DataType.Hiragana, "み");
                data.Dictionary.Add(DataType.Katakana, "ミ");
                data.Sprite = null;
                return data;
            case 32:
                data.Dictionary.Add(DataType.Hiragana, "む");
                data.Dictionary.Add(DataType.Katakana, "ム");
                data.Sprite = null;
                return data;
            case 33:
                data.Dictionary.Add(DataType.Hiragana, "め");
                data.Dictionary.Add(DataType.Katakana, "メ");
                data.Sprite = null;
                return data;
            case 34:
                data.Dictionary.Add(DataType.Hiragana, "も");
                data.Dictionary.Add(DataType.Katakana, "モ");
                data.Sprite = null;
                return data;
            case 35:
                data.Dictionary.Add(DataType.Hiragana, "ら");
                data.Dictionary.Add(DataType.Katakana, "ラ");
                data.Sprite = null;
                return data;
            case 36:
                data.Dictionary.Add(DataType.Hiragana, "り");
                data.Dictionary.Add(DataType.Katakana, "リ");
                data.Sprite = null;
                return data;
            case 37:
                data.Dictionary.Add(DataType.Hiragana, "る");
                data.Dictionary.Add(DataType.Katakana, "ル");
                data.Sprite = null;
                return data;
            case 38:
                data.Dictionary.Add(DataType.Hiragana, "れ");
                data.Dictionary.Add(DataType.Katakana, "レ");
                data.Sprite = null;
                return data;
            case 39:
                data.Dictionary.Add(DataType.Hiragana, "ろ");
                data.Dictionary.Add(DataType.Katakana, "ロ");
                data.Sprite = null;
                return data;
            case 40:
                data.Dictionary.Add(DataType.Hiragana, "や");
                data.Dictionary.Add(DataType.Katakana, "ヤ");
                data.Sprite = null;
                return data;
            case 41:
                data.Dictionary.Add(DataType.Hiragana, "ゆ");
                data.Dictionary.Add(DataType.Katakana, "ユ");
                data.Sprite = null;
                return data;
            case 42:
                data.Dictionary.Add(DataType.Hiragana, "よ");
                data.Dictionary.Add(DataType.Katakana, "ヨ");
                data.Sprite = null;
                return data;
            case 43:
                data.Dictionary.Add(DataType.Hiragana, "わ");
                data.Dictionary.Add(DataType.Katakana, "ワ");
                data.Sprite = null;
                return data;
            case 44:
                data.Dictionary.Add(DataType.Hiragana, "を");
                data.Dictionary.Add(DataType.Katakana, "ヲ");
                data.Sprite = null;
                return data;
            case 45:
                data.Dictionary.Add(DataType.Hiragana, "ん");
                data.Dictionary.Add(DataType.Katakana, "ン");
                data.Sprite = null;
                return data;
        }
        return null;
    }
}
