﻿public class KanjiCollection : DataCollection
{
    protected override IData GetData(int id)
    {
        Data data = new Data(); data.Category = Category.N5;
        switch (id)
        {
            case 0:
                data.Dictionary.Add(DataType.Kanji, "火");
                data.Dictionary.Add(DataType.Hiragana, "ひ");
                data.Dictionary.Add(DataType.Katakana, "カ");
                data.Sprite = null;
                return data;
            case 1:
                data.Dictionary.Add(DataType.Kanji, "水");
                data.Dictionary.Add(DataType.Hiragana, "みず");
                data.Dictionary.Add(DataType.Katakana, "スイ");
                data.Sprite = null;
                return data;
            case 2:
                data.Dictionary.Add(DataType.Kanji, "木");
                data.Dictionary.Add(DataType.Hiragana, "き");
                data.Dictionary.Add(DataType.Katakana, "モク");
                data.Sprite = null;
                return data;
            case 3:
                data.Dictionary.Add(DataType.Kanji, "金");
                data.Dictionary.Add(DataType.Hiragana, "かね");
                data.Dictionary.Add(DataType.Katakana, "キン");
                data.Sprite = null;
                return data;
            case 4:
                data.Dictionary.Add(DataType.Kanji, "土");
                data.Dictionary.Add(DataType.Hiragana, "つち");
                data.Dictionary.Add(DataType.Katakana, "ド");
                data.Sprite = null;
                return data;
            case 5:
                data.Dictionary.Add(DataType.Kanji, "東");
                data.Dictionary.Add(DataType.Hiragana, "ひがし");
                data.Dictionary.Add(DataType.Katakana, "トウ");
                data.Sprite = null;
                return data;
            case 6:
                data.Dictionary.Add(DataType.Kanji, "西");
                data.Dictionary.Add(DataType.Hiragana, "にし");
                data.Dictionary.Add(DataType.Katakana, "セイ");
                data.Sprite = null;
                return data;
            case 7:
                data.Dictionary.Add(DataType.Kanji, "南");
                data.Dictionary.Add(DataType.Hiragana, "みなみ");
                data.Dictionary.Add(DataType.Katakana, "ナン");
                data.Sprite = null;
                return data;
            case 8:
                data.Dictionary.Add(DataType.Kanji, "北");
                data.Dictionary.Add(DataType.Hiragana, "きた");
                data.Dictionary.Add(DataType.Katakana, "ホク");
                data.Sprite = null;
                return data;
            case 9:
                data.Dictionary.Add(DataType.Kanji, "月");
                data.Dictionary.Add(DataType.Hiragana, "つき");
                data.Dictionary.Add(DataType.Katakana, "ゲツ");
                data.Sprite = null;
                return data;
            case 10:
                data.Dictionary.Add(DataType.Kanji, "日");
                data.Dictionary.Add(DataType.Hiragana, "ひ");
                data.Dictionary.Add(DataType.Katakana, "ニチ");
                data.Sprite = null;
                return data;
        }
        return null;
    }
}
