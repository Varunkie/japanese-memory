﻿using System.Collections.Generic;

public class CustomCollection : DataCollection
{
    protected override IData GetData(int id)
    {
        return null;
    }

    public void Add(DataCollection collection)
    {
        Add(collection.Collection);
    }

    public void Add(List<Data> collection)
    {
        Add(collection.ToArray());
    }

    public void Add(IData[] collection)
    {
        foreach (IData item in collection)
            if (item != null)
                List.Add(item);
    }

    public void Clear()
    {
        List.Clear();
    }
}
