﻿using UnityEngine;

public class Database : MonoBehaviour
{
    #region Settings
    [Header("Timer Settings")]
    public Timer timer = new Timer() { mode = TimeMode.Counter, minutes = 1, seconds = 0 };
    [Header("Gameplay Settings")]
    public DataType pairOne = DataType.Kanji;
    public DataType pairTwo = DataType.Hiragana;
    [Header("Collection Settings")]
    public bool availableKanjiCollection = true;
    public bool availableKanaCollection = false;
    #endregion

    public static Database Instance { get; private set; }
    private CustomCollection _collection;

    #region Properties
    public DataType PairOne
    {
        get { return pairOne; }
    }
    public DataType PairTwo
    {
        get { return pairTwo; }
    }

    public IData[] Cards
    {
        get { return _collection.Collection; }
    }

    public Timer Timer
    {
        get { return timer; }
    }
    #endregion

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Initialize();
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this);
    }

    private void Initialize()
    {
        _collection = new CustomCollection();
        if (availableKanaCollection)
            _collection.Add(new KanaCollection());
        if (availableKanjiCollection)
            _collection.Add(new KanjiCollection());
    }
}
