﻿using UnityEngine;

public class Key
{
    public DataType Type { get; private set; }
    public IData Data { get; private set; }

    public Key(IData data, DataType type)
    {
        Type = type;
        Data = data;
    }

    public string Text
    {
        get { return Data.Text(Type); }
    }

    public Sprite Sprite
    {
        get { return Data.Sprite; }
    }
}
