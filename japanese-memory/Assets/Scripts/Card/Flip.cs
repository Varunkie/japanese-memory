﻿using System;
using System.Collections;
using UnityEngine;

public class Flip : MonoBehaviour
{
    #region Settings
    [Header("Flip Settings")]
    public bool allowClickOnSet = true;
    public bool allowClickOnFlipped = false;

    [Header("Animation Settings")]
    [Range(0.1f, 10f)] public float rotationTime = 0.2f;
    [Range(0.1f, 1f)] public float up = 0.5f;
    [Range(0.1f, 10f)] public float upTime = 0.1f;
    #endregion

    public event AnimationHandler AnimationChanged;

    public bool Flipped { get; private set; }
    private bool _animating;

    #region Properties
    public bool Animating
    {
        get { return _animating; }
        private set
        {
            if (_animating != value)
            {
                _animating = value;
                if (_animating)
                    OnAnimationChanged(AnimationState.Animating);
                else
                    OnAnimationChanged(AnimationState.End);
            }
        }
    }
    #endregion

    void Awake()
    {
        AnimationChanged += new AnimationHandler((object sender, AnimationEventArgs e) => { });
    }

    public void Begin()
    {
        if (!Animating)
            StartCoroutine(Up());
    }

    private bool Allow()
    {
        AnimationEventArgs e = new AnimationEventArgs(AnimationState.Starting);
        OnAnimationChanged(e);
        return e.Allow;
    }

    #region IEnumerator
    IEnumerator Up()
    {
        Animating = true;
        float yAcum = 0, yMove = (up / 10f) / upTime; ;
        while (true)
        {
            Vector3 pos = transform.position;
            if (yAcum + yMove <= up)
                pos.y += yMove;
            else
                pos.y = up;
            transform.position = pos;

            yAcum += yMove;
            if (yAcum < up)
                yield return new WaitForSeconds(0.1f);
            else
                break;
        }
        StartCoroutine(Rotation());
    }

    IEnumerator Rotation()
    {
        float zAcum = 0, zRotation = 1.8f / rotationTime;
        while (true)
        {
            float z = zAcum + zRotation <= 180f ? zRotation : 180f - zAcum;
            transform.Rotate(0f, 0f, z);

            zAcum += z;
            if (zAcum < 180f)
                yield return new WaitForSeconds(0.01f);
            else
                break;
        }
        Flipped = !Flipped;  StartCoroutine(Down());
    }

    IEnumerator Down()
    {
        float yAcum = transform.position.y, yMove = (up / 10f) / upTime; ;
        while (true)
        {
            Vector3 pos = transform.position;
            if (yAcum - yMove >= 0)
                pos.y -= yMove;
            else
                pos.y = 0f;
            transform.position = pos;

            yAcum -= yMove;
            if (yAcum > 0f)
                yield return new WaitForSeconds(0.1f);
            else
                break;
        }
        Animating = false; 
    }
    #endregion

    private void OnAnimationChanged(AnimationState state)
    {
        AnimationChanged(this, new AnimationEventArgs(state));
    }

    private void OnAnimationChanged(AnimationEventArgs e)
    {
        AnimationChanged(this, e);
    }

    void OnMouseDown()
    {
        if (((Flipped && allowClickOnFlipped) || (!Flipped && allowClickOnSet)) 
            && !Animating && Allow())
            Begin();
    }
}

public delegate void AnimationHandler(object sender, AnimationEventArgs e);
public class AnimationEventArgs : EventArgs
{
    public AnimationState State { get; private set; }
    public bool Allow { get; set; }

    public AnimationEventArgs(AnimationState state)
    {
        State = state;
        Allow = true;
    }
}

public enum AnimationState
{
    Starting,
    Animating,
    End
}