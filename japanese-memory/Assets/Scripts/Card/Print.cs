﻿using UnityEngine;
using UnityEngine.UI;

public class Print : MonoBehaviour
{
    #region
    [Header("Print Setting")]
    public string printText;
    public Sprite bottomImage;
    #endregion

    private SpriteRenderer _spriteRenderer;
    private Text _textComponent;

    private float _xOriginalScale;

    #region Properties
    public Sprite Image
    {
        set { _spriteRenderer.sprite = value; }
    }

    public string Text
    {
        set { _textComponent.text = value; Adjust(); }
        get { return _textComponent.text; }
    }
    #endregion

    void Awake()
    {
        _textComponent = GetComponentInChildren<Text>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        _xOriginalScale = _textComponent.transform.localScale.x;

        Text = printText;
        Image = bottomImage;
    }

    private void Adjust()
    {
        if (_textComponent.text.Length > 0)
        {
            float xSize = _textComponent.preferredWidth;
            if (xSize > 0f)
            {
                Vector3 scale = _textComponent.transform.localScale;
                scale.x = (_textComponent.fontSize / xSize) * _xOriginalScale;
                if (scale.x > _xOriginalScale)
                    scale.x = _xOriginalScale;
                _textComponent.transform.localScale = scale;
            }
        }
    }
}
