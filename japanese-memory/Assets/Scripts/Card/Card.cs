﻿using UnityEngine;

public class Card : MonoBehaviour, ICard
{
    #region Settings
    [Header("Pair Settings")]
    public Color foundColor;
    #endregion

    private Select Selecter { get; set; }
    private Flip Fliper { get; set; }
    private Print Printer { get; set; }

    public event SelectHandler Selecting;
    public event AnimationHandler AnimationChanged;

    private Key _key;

    #region Properties
    public Key Key
    {
        get { return _key; }
        set
        {
            _key = value;
            Printer.Text = _key.Text;
            if (_key.Type == DataType.Sprite)
                Printer.Image = _key.Sprite;
        }
    }

    public bool Loaded
    {
        get { return Key != null; }
    }
    #endregion

    void Awake()
    {
        Selecting += new SelectHandler((object sender, SelectEventArgs e) => { });
        AnimationChanged += new AnimationHandler((object sender, AnimationEventArgs e) => { });

        Printer = GetComponentInChildren<Print>(); 
        Selecter = GetComponent<Select>(); Selecter.Selecting += Select_Selecting;
        Fliper = GetComponent<Flip>(); Fliper.AnimationChanged += Flip_AnimationChanged;
    }

    public void Set(IData data, DataType type)
    {
        Key = new Key(data, type);
    }

    public bool Pair(ICard other)
    {
        if (!Loaded || !other.Loaded)
            return false;
        else if (Key.Type == DataType.Sprite || other.Key.Type == DataType.Sprite)
            return Key.Data.Sprite == other.Key.Data.Sprite;
        else
            return Key.Data.Text(Key.Type) == other.Key.Data.Text(Key.Type) ||
                Key.Data.Text(other.Key.Type) == other.Key.Data.Text(other.Key.Type);
    }

    public void Found()
    {
        Selecter.Color(foundColor);
    }

    public void NotFound()
    {
        if (Fliper.Flipped)
            Fliper.Begin();
        Selecter.Unselect();
    }

    #region Events
    private void Select_Selecting(object sender, SelectEventArgs e)
    {
        Selecting(this, e);
        if (e.Allow)
            Fliper.Begin();
    }

    private void Flip_AnimationChanged(object sender, AnimationEventArgs e)
    {
        AnimationChanged(this, e);
    }
    #endregion
}

public interface ICard
{
    bool Loaded { get; }
    Key Key { get; }

    event SelectHandler Selecting;
    event AnimationHandler AnimationChanged;

    void Set(IData data, DataType type);
    bool Pair(ICard other);
    void Found();
    void NotFound();
}