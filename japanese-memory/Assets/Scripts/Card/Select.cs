﻿using System;
using UnityEngine;

public class Select : MonoBehaviour
{
    #region Settings
    public GameObject model;

    [Header("Selection Settings")]
    public bool blockedOnClick = true;

    [Header("Color Settings")]
    public Color selectingColor;
    public Color selectedColor;
    #endregion

    public event SelectHandler Selecting;

    private Renderer[] _renderers; private Color[] _colors;

    public bool Colored { get; private set; }
    public bool Selected { get; private set; }
    public bool Blocked { get; private set; }

    void Awake()
    {
        Selecting += new SelectHandler((object sender, SelectEventArgs e) => { });

        _renderers = model.GetComponentsInChildren<Renderer>();
        _colors = new Color[_renderers.Length];
        for (int i = 0; i < _colors.Length; i++)
            _colors[i] = _renderers[i].material.color;
    }

    public void Reset()
    {
        Descolor();

        Blocked = false;
        Colored = false;
    }

    private void Descolor()
    {
        Colored = false;
        for (int i = 0; i < _renderers.Length; i++)
            _renderers[i].material.color = _colors[i];
    }

    public void Color(Color c)
    {
        Colored = true;
        for (int i = 0; i < _renderers.Length; i++)
            _renderers[i].material.color = c;
    }

    public void Unselect()
    {
        Blocked = false; Selected = false;
        Descolor();
    }

    private bool Allow()
    {
        SelectEventArgs e = new SelectEventArgs();
        OnSelecting(e);
        return e.Allow;
    }

    private void OnSelecting(SelectEventArgs e)
    {
        Selecting(this, e);
    }

    void OnMouseDown()
    {
        if (!Blocked && !Selected && Allow())
        {
            Selected = true;
            Color(selectedColor);
            if (blockedOnClick)
                Blocked = true;
        }
    }

    void OnMouseUp()
    {
        if (!Blocked && Selected)
        {
            Selected = false;
            Descolor();
        }
    }

    void OnMouseOver()
    {
        if (!Blocked && !Colored)
            Color(selectingColor);
    }

    void OnMouseExit()
    {
        if (!Blocked && Colored)
            Descolor();
    }
}

public delegate void SelectHandler(object sender, SelectEventArgs e);
public class SelectEventArgs : EventArgs
{
    public bool Allow { get; set; }

    public SelectEventArgs()
    {
        Allow = true;
    }
}
