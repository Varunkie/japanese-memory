﻿using UnityEngine.UI;
using UnityEngine;

public class TextReceiver : MonoBehaviour
{
    #region Settings
    [Header("Target Setting")]
    public GameObject owner;
    public string property;
    #endregion

    private Text _component;

    #region Properties
    public string Text
    {
        get { return _component.text; }
        set { _component.text = value; }
    }
    #endregion

    void Awake()
    {
        _component = GetComponentInChildren<Text>();
        if (owner != null)
            owner.GetComponent<IPropertyOwner>().PropertyChanged += Owner_PropertyChanged;
    }

    #region Events
    private void Owner_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.IsProperty(property))
            Text = e.Value;
    }
    #endregion
}
