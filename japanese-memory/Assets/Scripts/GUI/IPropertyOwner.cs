﻿using System;

public interface IPropertyOwner
{
    event PropertyChangedHandler PropertyChanged;
}

public delegate void PropertyChangedHandler(object sender, PropertyChangedEventArgs e);
public class PropertyChangedEventArgs : EventArgs
{
    public string Property { get; private set; }
    public string Value { get; private set; }

    public PropertyChangedEventArgs(string property, string value)
    {
        Value = value;
        Property = property;
    }

    public bool IsProperty(string property)
    {
        return Property == property;
    }
}
