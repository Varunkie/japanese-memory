﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReplayButton : MonoBehaviour
{
    #region Settings
    [Header("Replay Settings")]
    public string sceneName = "MainScene";
    #endregion

    public void Replay()
    {
        SceneManager.LoadScene(sceneName);
    }
}
