﻿using System.Collections.Generic;
using UnityEngine;

public class Dealer : MonoBehaviour
{
    #region Settings
    [Header("Card Settings")]
    public GameObject cardCollection;
    #endregion

    private ICard[] _cards;

    #region Properties
    public int NumberOfCards
    {
        get { return _cards.Length; }
    }

    public int Couples
    {
        get { return _cards.Length / 2; }
    }

    public bool Playable
    {
        get { return _cards.Length % 2 == 0 && Database.Instance.Cards.Length >= Couples; }
    }
    #endregion

    void Awake()
    {
        _cards = RandomSoft(cardCollection.GetComponentsInChildren<ICard>());
    }

    void Start ()
    {
        if (Playable)
            Initialize();
        else
            Debug.Log("Game not playable, Couples [" + Couples + "] and Database Data Length [" + Database.Instance.Cards.Length + "]");
	}
	
    private void Initialize()
    {
        IData[] collection = Draw(Shuffle(Database.Instance.Cards));
        if (collection.Length >= Couples)
            Set(Draw(collection));
        else
            Debug.Log("Game not playable, Collection Specific Type [" + collection.Length + "] not sufficient");
    }

    private void Set(IData[] data)
    {
        for (int i = 0, j = 0; i < _cards.Length && j < data.Length; i += 2, j++)
        {
            _cards[i].Set(data[j], Database.Instance.PairOne);
            _cards[i + 1].Set(data[j], Database.Instance.PairTwo);
        }
    }

    private IData[] Draw(IData[] collection)
    {
        List<IData> aux = new List<IData>();
        foreach (IData item in collection)
            if (aux.Count < Couples && item.Contains(Database.Instance.PairOne) && item.Contains(Database.Instance.PairTwo))
                aux.Add(item);
        return aux.ToArray();
    }

    private IData[] Shuffle(IData[] data)
    {
        return RandomSoft(data);
    }

    #region Static
    public static T[] RandomSoft<T>(T[] array)
    {
        return RandomSoftWithTrim(array, array.Length);
    }

    public static T[] RandomSoftWithTrim<T>(T[] array, int length)
    {
        List<T> list = new List<T>();
        int index = length - 1;

        while (index >= 0 && length > 0)
        {
            int ind = Random.Range(0, index);
            list.Add(array[ind]);

            T aux = array[index];
            array[index] = array[ind];
            array[ind] = aux;

            index--; length--;
        }
        return list.ToArray();
    }
    #endregion
}
