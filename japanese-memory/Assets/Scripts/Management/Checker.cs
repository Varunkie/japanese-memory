﻿using System;
using UnityEngine;

public class Checker : MonoBehaviour
{
    #region Settings
    [Header("Checker Settings")]
    public GameObject cardCollection;
    [Range(0.1f, 5f)] public float checkTime = 1f;
    #endregion

    private ICard FirstSelected { get; set; }
    private ICard SecondSelected { get; set; }

    private bool Blocked { get; set; }
    public bool Stop { get; set; }
    private bool FirstChoice { get; set; }
    
    public event CheckerHandler Checked;

    void Awake()
    {
        Checked += new CheckerHandler((object sender, CheckerEventArgs e) => { });
    }

    void Start()
    {
        foreach (ICard item in cardCollection.GetComponentsInChildren<ICard>())
            item.Selecting += Card_Selecting;
    }

    private void Select(ICard card)
    {
        if (!Blocked)
        {
            if (FirstSelected == null)
                FirstSelected = card;
            else if (SecondSelected == null)
                SecondSelected = card;
            if (!FirstChoice)
            {
                FirstChoice = true;
                OnChecked(CheckerState.FirstChoice);
            }

            if (FirstSelected != null && SecondSelected != null)
            {
                Blocked = true; Invoke("Check", checkTime);
            }
        }
    }

    private void Check()
    {
        if (FirstSelected.Pair(SecondSelected))
        {
            OnChecked(CheckerState.Found);
            FirstSelected.Found(); SecondSelected.Found();
            FirstSelected = null; SecondSelected = null;
            Blocked = false;
        }
        else
        {
            OnChecked(CheckerState.NotFound);
            FirstSelected.AnimationChanged += FirstSelected_AnimationChanged;
            SecondSelected.AnimationChanged += SecondSelected_AnimationChanged;
            FirstSelected.NotFound(); SecondSelected.NotFound();
        }
    }

    private void OnChecked(CheckerState state)
    {
        Checked(this, new CheckerEventArgs(state, FirstSelected, SecondSelected));
    }

    #region Events
    private void Card_Selecting(object sender, SelectEventArgs e)
    {
        if (Stop || Blocked)
            e.Allow = false;
        else if (FirstSelected == null || SecondSelected == null)
            Select(sender as ICard);
    }

    private void FirstSelected_AnimationChanged(object sender, AnimationEventArgs e)
    {
        if (e.State == AnimationState.End)
        {
            FirstSelected.AnimationChanged -= FirstSelected_AnimationChanged;
            FirstSelected = null; Blocked = !(FirstSelected == null && SecondSelected == null);
        }
    }

    private void SecondSelected_AnimationChanged(object sender, AnimationEventArgs e)
    {
        if (e.State == AnimationState.End)
        {
            SecondSelected.AnimationChanged -= SecondSelected_AnimationChanged;
            SecondSelected = null; Blocked = !(FirstSelected == null && SecondSelected == null);
        }
    }
    #endregion
}

public delegate void CheckerHandler(object sender, CheckerEventArgs e);
public class CheckerEventArgs : EventArgs
{
    public ICard FirstSelected { get; private set; }
    public ICard SecondSelected { get; private set; }
    public CheckerState State { get; private set; }

    public CheckerEventArgs(CheckerState state, ICard first, ICard second)
    {
        State = state;
        FirstSelected = first;
        SecondSelected = second;
    }
}

public enum CheckerState
{
    FirstChoice,
    Found,
    NotFound
}