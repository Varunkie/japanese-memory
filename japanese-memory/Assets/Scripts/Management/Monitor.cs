﻿using UnityEngine;

public class Monitor : MonoBehaviour, IPropertyOwner
{
    #region Settings
    [Header("Monitor Settings")]
    public GameObject replayButton;
    #endregion

    public event PropertyChangedHandler PropertyChanged;

    private Checker Checker { get; set; }
    private Dealer Dealer { get; set; }

    private int _numberOfPairs;
    private Timer _timer;

    #region Properties
    private bool AllFound
    {
        get { return _numberOfPairs >= Dealer.Couples; }
    }

    private int NumberOfPairs
    {
        get { return _numberOfPairs; }
        set
        {
            _numberOfPairs = value; OnNumberOfPairsChanged();
            if (AllFound)
                End();
        }
    }
    #endregion

    private void Awake()
    {
        PropertyChanged += new PropertyChangedHandler((object sender, PropertyChangedEventArgs e) => { });

        Checker = GetComponent<Checker>(); Checker.Checked += Checker_Checked;
        Dealer = GetComponent<Dealer>();
    }

    private void Start()
    {
        _timer = Database.Instance.Timer; _timer.Stop += Timer_Stop;
        _timer.Enabled = false;

        OnTimerChanged(); OnNumberOfPairsChanged();

        replayButton.SetActive(false);   
    }

    private void Update()
    {
        if (_timer.Tick(Time.deltaTime))
            OnTimerChanged();
    }

    private void OnTimerChanged()
    {
        PropertyChanged(this, new PropertyChangedEventArgs("Timer", _timer.Format));
    }

    private void OnNumberOfPairsChanged()
    {
        PropertyChanged(this, new PropertyChangedEventArgs("Pairs", NumberOfPairs.ToString()));
    }

    private void End()
    {
        _timer.Enabled = false;
        Checker.Stop = true;
        replayButton.SetActive(true);
    }

    #region Events
    private void Checker_Checked(object sender, CheckerEventArgs e)
    {
        if (e.State == CheckerState.Found)
            NumberOfPairs++;
        else if (e.State == CheckerState.FirstChoice)
            _timer.Enabled = true;
    }

    private void Timer_Stop(object sender, TimerEventArgs e)
    {
        End();
    }
    #endregion
}
