﻿using System;
using UnityEngine;

[Serializable]
public struct Timer
{
    #region Settings
    public TimeMode mode;
    [Range(1, 10)] public int minutes;
    [Range(0, 59)] public int seconds;
    #endregion

    public event TimerHandler Stop;

    public bool Enabled { get; set; }
    private float _deltaTime;

    #region Properties
    public TimeMode Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public int Seconds
    {
        get { return seconds; }
    }

    public int Minutes
    {
        get { return minutes; }
    }

    public string Format
    {
        get
        {
            string format = minutes.ToString() + ":";
            if (seconds < 10)
                format += "0";
            format += seconds.ToString();
            return format;
        }
    }
    #endregion

    private void Up()
    {
        seconds++;
        if (seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }
    }

    private void Down()
    {
        seconds--;
        if (seconds < 0)
        {
            if (minutes > 0)
            {
                seconds = 59;
                minutes--;
            }
            else
            {
                seconds = 0;
                Enabled = false; OnStop();
            }
        }
    }

    public bool Tick(float delta)
    {
        bool tick = false;
        if (Enabled)
        {
            _deltaTime += delta;
            while (_deltaTime >= 1f)
            {
                _deltaTime -= 1f; tick = true;
                switch (Mode)
                {
                    case TimeMode.Counter:
                        Down();
                        break;
                    case TimeMode.Watch:
                        Up();
                        break;
                }
            }
        }
        return tick;
    }

    private void OnStop()
    {
        if (Stop != null)
            Stop(this, new TimerEventArgs());
    }
}

public enum TimeMode
{
    Watch,
    Counter
}

public delegate void TimerHandler(object sender, TimerEventArgs e);
public class TimerEventArgs : EventArgs
{
    public TimerEventArgs()
    {

    }
}